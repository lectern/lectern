// Copyright 2012 Ruben Pollan <meskio@sindominio.net>
//
// This file is part of Lectern.
//
// Lectern is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lectern is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lectern.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	. "git.gitorious.org/go-pkg/epubgo.git"
)

const (
	title = "title"
	authors = "creator"
)

type EpubBook struct {
	f   *Epub
	it  *SpineIterator
}

func NewEpubBook(path string) (b EpubBook, err error) {
	b.f, err = Open(path)
	if err != nil {
		return
	}

	b.it, err = b.f.Spine()
	return
}

func (b EpubBook) Title() string {
	t, err := b.f.Metadata(title)
	if err != nil || len(t) == 0 {
		return ""
	}
	return t[0]
}

func (b EpubBook) Authors() []string {
	a, err := b.f.Metadata(authors)
	if err != nil {
		return nil
	}
	return a
}

func (b EpubBook) Elements() Content {
	reader, err := b.it.Open()
	if err != nil {
		return nil //FIXME
	}
	defer reader.Close()
	c, _ := Parse(reader)
	return c
}

func (b *EpubBook) NextSection() bool {
	return b.it.Next() == nil
}

func (b *EpubBook) PrevSection() bool {
	return b.it.Previous() == nil
}

func (b EpubBook) IsFirstSection() bool {
	return b.it.IsFirst()
}

func (b EpubBook) IsLastSection() bool {
	return b.it.IsLast()
}

func (b EpubBook) Close() {
	b.f.Close()
}
