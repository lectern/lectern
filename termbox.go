// Copyright 2012 Ruben Pollan <meskio@sindominio.net>
//
// This file is part of Lectern.
//
// Lectern is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lectern is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lectern.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	. "github.com/nsf/termbox-go"
)

func SetStr(x, y int, str string, fg, bg Attribute) (int, int) {
	cols, lines := Size()
	for _, r := range str {
		if x >= cols {
			x = 0
			y++
		}
		if y >= lines {
			break
		}
		if r != '\n' {
			SetCell(x, y, r, fg, bg)
			x++
		} else {
			SetBlanks(x, y, cols-x)
			y++
			x = 0
		}
	}
	return x, y
}

func Scroll(n int) {
	buff := CellBuffer()
	cols, _ := Size()
	switch {
	case n > 0:
		for i, r := range buff {
			if i < cols*n {
				continue
			}
			buff[i-cols*n] = r
		}
	case n < 0:
		for i := len(buff) + cols*n - 1; i >= 0; i-- {
			buff[i-cols*n] = buff[i]
		}
	}
}

func SetBlanks(x, y, cols int) {
	for i := x; i < x+cols; i++ {
		SetCell(i, y, ' ', ColorDefault, ColorDefault)
	}
}

func Str2Cell(str string, fg, bg Attribute) []Cell {
	c := make([]Cell, len(str))
	for i, r := range str {
		c[i].Ch = r
		c[i].Fg = fg
		c[i].Bg = bg
	}
	return c
}
