// Copyright 2012 Ruben Pollan <meskio@sindominio.net>
//
// This file is part of Lectern.
//
// Lectern is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lectern is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lectern.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"encoding/xml"
	"io"
	"strings"
	//"fmt"
	//"os"
)

type EpubContent struct {
	elements []element
	i        int
}

type element struct {
	content     string
	elementType ElementType
}

type parse struct {
	content EpubContent
	tok     xml.Token
	store   bool
	str     string
	//log *os.File
}

func Parse(reader io.Reader) (*EpubContent, error) {
	var p parse
	//p.log, _ = os.OpenFile("foo", os.O_WRONLY, 0x0666)
	//defer p.log.Close()
	d := xml.NewDecoder(reader)
	d.Strict = false
	d.AutoClose = xml.HTMLAutoClose
	d.Entity = xml.HTMLEntity
	var err error
	p.tok, err = d.Token()
	if err != nil {
		return nil, err
	}
	for p.tok != nil {
		p.token()
		p.tok, _ = d.Token()
	}
	return &p.content, err
}

func (p *parse) token() {
	switch se := p.tok.(type) {
	case xml.StartElement:
		//fmt.Fprintln(p.log, se.Name.Local)
		switch se.Name.Local {
		case "p", "h1", "h2", "h3", "h4", "h5", "h6":
			p.store = true
			p.str = ""
		case "br":
			if p.store {
				p.str += "\n"
			}
		}
	case xml.CharData:
		if p.store {
			str := string(se)
			str = strings.Replace(str, "\n", "", -1)
			p.str += str
		}
		//fmt.Fprintln(p.log, "->",string(se))
	case xml.EndElement:
		switch se.Name.Local {
		case "p":
			if !p.store || len(p.str) == 0 {
				break
			}
			e := element{p.str, PARAGRAPH}
			p.content.elements = append(p.content.elements, e)
			p.store = false
		case "h1", "h2", "h3", "h4", "h5", "h6":
			if !p.store {
				break
			}
			eType := TITLE
			e := element{p.str, eType}
			p.content.elements = append(p.content.elements, e)
			p.store = false
		}
		//fmt.Fprintln(p.log, "/",se.Name.Local)
	}
}

func (c EpubContent) Type() ElementType {
	if c.i >= len(c.elements) || c.i < 0 {
		return NONE
	}
	return c.elements[c.i].elementType
}

func (c EpubContent) Text() string {
	if c.i >= len(c.elements) || c.i < 0 {
		return ""
	}
	return c.elements[c.i].content
}

func (c *EpubContent) Next() bool {
	if c.i >= len(c.elements)-1 {
		return false
	}
	c.i++
	return true
}

func (c *EpubContent) Prev() bool {
	if c.i <= 0 {
		return false
	}
	c.i--
	return true
}

func (c EpubContent) IsFirst() bool {
	return c.i <= 0
}

func (c EpubContent) IsLast() bool {
	return c.i >= len(c.elements)-1
}

func (c EpubContent) Clone() Content {
	return &EpubContent{c.elements, c.i}
}
