// Copyright 2012 Ruben Pollan <meskio@sindominio.net>
//
// This file is part of Lectern.
//
// Lectern is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lectern is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lectern.  If not, see <http://www.gnu.org/licenses/>.

package main

import "testing"

const (
	section_title = "License"
)

func TestEpubContent(t *testing.T) {
	e, _ := NewEpubBook(book_path)
	e.NextSection()
	e.NextSection()
	c := e.Elements()

	if !c.IsFirst() {
		t.Errorf("It's not the first element")
	}
	if c.Type() != TITLE {
		t.Errorf("It's not the title")
	}
	if c.Text() != section_title {
		t.Errorf("Wrong text:", c.Text())
	}
	if !c.Next() {
		t.Errorf("Can not go to the next element")
	}
	if c.IsFirst() {
		t.Errorf("It's the first element")
	}
	if !c.Prev() {
		t.Errorf("Can not go to previous element")
	}
	if !c.IsFirst() {
		t.Errorf("It's not the first element")
	}
}

func TestEpubContentClone(t *testing.T) {
	e, _ := NewEpubBook(book_path)
	e.NextSection()
	e.NextSection()
	c1 := e.Elements()
	c2 := c1.Clone()

	c2.Next()
	if c1.Text() != section_title {
		t.Errorf("Wrong text:", c1.Text())
	}
}
