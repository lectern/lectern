// Copyright 2012 Ruben Pollan <meskio@sindominio.net>
//
// This file is part of Lectern.
//
// Lectern is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lectern is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lectern.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	. "github.com/nsf/termbox-go"
	"log"
)

type cursor struct {
	b       Book
	first   Content
	firstCh int
	last    Content
	lastCh  int
}

// print the content and returns x, y at the end of writing and l with the lenght written
func printContent(line, start, length int, c Content) (x, y, l int) {
	cols, lines := Size()
	if line >= lines {
		return 0, line, 0
	}

	switch c.Type() {
	case PARAGRAPH:
		str := c.Text()
		stop := start + length
		if stop > len(str) {
			stop = len(str)
		}
		x, y = SetStr(0, line, str[start:stop], ColorDefault, ColorDefault)
		l = stop - start
		if x != 0 {
			SetBlanks(x, y, cols-x)
		}
	case TITLE:
		// FIXME: if title is longer than one line
		str := c.Text()
		stop := start + cols
		if stop > len(str) {
			stop = len(str)
		}
		x = (cols - (stop - start)) / 2
		SetBlanks(0, line, cols)
		x, y = SetStr(x, line, str[start:stop], AttrBold, ColorDefault)
		l = cols
	}
	return
}

/* Draw all the screen with the cursor */
func (c *cursor) drawScreen() {
	Clear(ColorDefault, ColorDefault)
	cols, lines := Size()
	c.last = c.first.Clone()
	c.firstCh = 0
	c.lastCh = 0

	var x, y, l int
	for y < lines {
		size := (lines - y) * cols
		x, y, l = printContent(y, 0, size-1, c.last)
		if x != 0 && y == lines-1 || !c.last.Next() {
			c.lastCh = l
			break
		}
		SetBlanks(x, y+1, cols)
		y += 2
	}
	Flush()
}

func (c *cursor) lineDown() {
	cols, lines := Size()

	/* print new line */
	p := c.last.Text()
	if len(p) <= c.lastCh {
		if !c.last.Next() {
			return
		}
		c.lastCh = 0
		Scroll(1)
		SetBlanks(0, lines-1, cols)
	} else {
		Scroll(1)
		_, _, l := printContent(lines-1, c.lastCh, cols, c.last)
		c.lastCh += l
	}
	Flush()

	/* update c.first */
	if c.firstCh != 0 && c.firstCh >= len(c.first.Text()) {
		if c.first.Next() {
			c.firstCh = 0
		}
	} else {
		c.firstCh += cols
	}
}

func (c *cursor) lineUp() {
	cols, _ := Size()

	/* print new line */
	c.firstCh -= cols
	if c.firstCh < 0 {
		if !c.first.Prev() {
			c.firstCh = 0
			return
		}
		l := len(c.first.Text())
		c.firstCh = l + cols - l%cols
		Scroll(-1)
		SetBlanks(0, 0, cols)
	} else {
		Scroll(-1)
		printContent(0, c.firstCh, cols, c.first)
	}
	Flush()

	/* update c.last */
	c.lastCh -= cols
	if c.lastCh < 0 && c.last.Prev() {
		l := len(c.last.Text())
		c.lastCh = l + cols - l%cols
	}
}

func UI(b Book) {
	err := Init()
	if err != nil {
		log.Fatal(err)
	}
	defer Close()
	HideCursor()

	var c cursor
	c.b = b
	c.first = b.Elements()
	c.drawScreen()

loop:
	for {
		switch ev := PollEvent(); ev.Type {
		case EventKey:
			switch {
			case ev.Key == KeyEsc || ev.Ch == 'q':
				break loop
			case ev.Key == KeyArrowDown || ev.Ch == 'j':
				c.lineDown()
			case ev.Key == KeyArrowUp || ev.Ch == 'k':
				c.lineUp()
			case ev.Key == KeyPgdn || ev.Key == KeySpace:
				if c.last.IsLast() {
					c.b.NextSection()
					c.first = c.b.Elements()
				} else {
					c.first = c.last.Clone()
				}
				c.drawScreen()
			case ev.Key == KeyPgup || ev.Key == KeyBackspace || ev.Key == KeyBackspace2:
				if c.first.IsFirst() {
					c.b.PrevSection()
					c.first = c.b.Elements()
				} else {
					cols, lines := Size()
					size := cols*lines - len(c.first.Text())
					for size > 0 {
						if !c.first.Prev() {
							break
						}
						size -= len(c.first.Text()) + cols
					}
					if !c.first.IsFirst() {
						c.first.Next()
					}
				}
				c.drawScreen()
			}
		case EventResize:
			c.drawScreen()
		}
	}
}
