// Copyright 2012 Ruben Pollan <meskio@sindominio.net>
//
// This file is part of Lectern.
//
// Lectern is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lectern is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lectern.  If not, see <http://www.gnu.org/licenses/>.

package main

type ElementType int

const (
	NONE ElementType = iota
	PARAGRAPH
	TITLE
)

type Book interface {
	Title() string
	Authors() []string
	Elements() Content
	NextSection() bool
	PrevSection() bool
	IsFirstSection() bool
	IsLastSection() bool
	Close()
}

type Content interface {
	Type() ElementType
	Text() string
	Next() bool
	Prev() bool
	IsFirst() bool
	IsLast() bool
	Clone() Content
}
