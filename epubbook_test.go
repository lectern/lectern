// Copyright 2012 Ruben Pollan <meskio@sindominio.net>
//
// This file is part of Lectern.
//
// Lectern is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lectern is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lectern.  If not, see <http://www.gnu.org/licenses/>.

package main

import "testing"

const (
	book_path = "testdata/Cory_Doctorow_-_Little_Brother.epub"
	book_title = "Little Brother"
	book_author = "Doctorow, Cory"
)

func TestEpubBook(t *testing.T) {
	e, err := NewEpubBook(book_path)
	if err != nil {
		t.Errorf("NewEpubBook(%v) return an error: %v", book_path, err)
	}

	if !e.IsFirstSection() {
		t.Errorf("It's not the first section")
	}
	if e.IsLastSection() {
		t.Errorf("It's the last section")
	}
	if !e.NextSection() {
		t.Errorf("Can not go to the next section")
	}
	if e.IsFirstSection() {
		t.Errorf("It's the first section")
	}
	if !e.PrevSection() {
		t.Errorf("Can not go to previous section")
	}
	if !e.IsFirstSection() {
		t.Errorf("It's not the first section")
	}
}

func TestEpubBookTitle(t *testing.T) {
	e, _ := NewEpubBook(book_path)
	if e.Title() != book_title {
		t.Errorf("Not valid title: %v", e.Title())
	}
}

func TestEpubBookAuthor(t *testing.T) {
	e, _ := NewEpubBook(book_path)
	if e.Authors()[0] != book_author {
		t.Errorf("Not valid author: %v", e.Authors()[0])
	}
}
